#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

int dictLength = 0;
int hashesLength = 0;
int testCounter = 0;

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5

    // Compare the two hashes

    // Free any malloc'd memory
    
    int plainPassLength = strlen(guess);
    
    char* plainPassToHash =  md5(guess, plainPassLength);
    
    if (strcmp(hash, plainPassToHash) == 0) {
        
        printf("%d. Matched: %s\n", ++testCounter, guess);
        
        return 1;
    } else {
        return 0;
    }
    
    free(plainPassToHash);

}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
// char **readfile(char *filename)
// {
//     return NULL;
// }

char ** readfile(char *filename, int isDictionary)
{
    // Malloc space for entire file
    // Get size pf the file
    struct stat st;
    if (stat(filename, &st) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
    }
    int len = st.st_size;
    
    char *file = malloc(len);
    
    // Read etire file into memory
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        printf("Can't open %s for read\n", filename);
        exit(1);
    }
    fread(file, 1, len, f);
    fclose(f);
    
    // Replace \n with \0
    int count = 0;
    for (int i = 0; i < len; i++)
    {
        if (file[i] == '\n')
        {
            file[i] = '\0';
            count++;
        }
    }
    
    if (isDictionary == 1) {
        dictLength = count;
    } else {
        hashesLength = count;
    }
    
    // Malloc space for array of pointers
    char ** line = malloc((count+1) * sizeof(char *));
    
    // Fill in addresses
    int word = 0;
    line[word] = file; // The first word in the file
    word++;
    for (int i = 1; i < len; i++)
    {
        if (file[i] == '\0' && i + 1 < len)
        {
            line[word] = &file[i+1];
            word++;
        }
    }
    
    line[word] = NULL;
    
    //Return address of second array
    // *size = word-1;
    return line;
}


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    

    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1], 0);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2], 1);
    
    printf("\n%s lines: %d\n", argv[1], hashesLength);
    printf("%s lines: %d\n", argv[2], dictLength);
    
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    for (int i = 0; i < hashesLength; i++) {
        
        char* currenStolenHash = hashes[i];
        
        for (int j = 0; j < dictLength; j++) {
            
            char* currentPlainFromDict = dict[j];
            
            tryguess(currenStolenHash, currentPlainFromDict);
        }
    }
    testCounter = 0;
}

// in order to make it faster you can prehash all that passwords and store in anoher array
